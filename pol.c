#include <math.h>
#include <stdio.h>
#include <complex.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

typedef struct polynom {
    size_t order;
    double complex *a;
    double complex *x;
} pol_t;

double randd() {
    return (((double)rand())/((double)RAND_MAX));
}

void pol_init(pol_t * pol, size_t order)
{
    size_t i;
    pol->order = order;
    pol->a = calloc(1+order, sizeof *pol->a);
    pol->x = calloc(1+order, sizeof *pol->x);
    for (i = 0; i<= order; ++i) {
        pol->a[i] = rand() % 10 + rand()%10*I;
    }
}

void pol_free(pol_t p)
{
    free(p.a);
    p.a = NULL;
    free(p.x);
    p.x = NULL;
}

void pol_dump(pol_t pol, char *name)
{
    size_t i, j;
    if (NULL == name)
        name = "P";
    printf("%s order %lu\n", name, pol.order);
    printf("%s(x) = ", name);
    for (i = 0, j = pol.order; i <= pol.order; ++i, --j)
    {
        if (i) printf("+");
        if (j>1)
            printf("(%.2f + %.2fi)x^%lu", creal(pol.a[j]), cimag(pol.a[j]), j);
        else if (j)
            printf("(%.2f + %.2fi)x", creal(pol.a[j]), cimag(pol.a[j]));
        else
            printf("(%.2f + %.2fi)", creal(pol.a[j]), cimag(pol.a[j]));
    }
    printf("\n%s(x) = ", name);
    for (i = 0; i < pol.order; ++i)
    {
        printf("(x-(%.2f + %.2fi))", creal(pol.x[i]), cimag(pol.x[i]) );
    }
    printf("\n");
}

double complex pol_eval(pol_t p, double complex x)
{
    size_t i, j;
    double complex X = 1;
    double complex ret = 0;
    for (i=0,j=p.order;i<=p.order;++i,--j) {
        ret += p.a[i] * X;
        X *= x;
    }
    return ret;
}

void pol_get_derivative(pol_t p, pol_t * q)
{
    size_t i;
    size_t order = p.order;

    pol_init(q, order-1);

    for (i = 1; i <= p.order; ++i) {
        q->a[i-1] = p.a[i] * i;
    }
}

void pol_factorize(pol_t p, double complex root, pol_t *q)
{
    size_t i;
    size_t order = p.order;

    pol_init(q, order-1);

    q->a[order-1] = p.a[order];
    for (i = order-1; i > 0; --i)
    {
        q->a[i-1] = q->a[i] * root + p.a[i];
    }
}

double complex pol_find_root(pol_t p, double complex hint, size_t retry)
{
    size_t i;
    pol_t q;
    double complex x = hint;
    double complex xprev = x;

    pol_get_derivative(p, &q);


    while (1) {
        for (i = 0; i < 100; ++i)
        {
            xprev = x;
            x = xprev - pol_eval(p, xprev) / pol_eval(q, xprev);
        }
        if (!retry--)
            break;
        if (isnan(cabs(x))||(cabs(x-xprev)>0.01 ) ) {
            x = 20.*randd() + 20.*randd()*I;
        }
        else {break;}
    }

    return x;
}

void pol_copy(pol_t p, pol_t *q)
{
    pol_init(q, p.order);
    memcpy(q->a, p.a, (p.order+1)*sizeof *p.a);
    memcpy(q->x, p.x, (p.order+1)*sizeof *p.x);
}

void pol_find_all_roots(pol_t p)
{
    size_t i;
    pol_t q, r;

    pol_copy(p, &q);

    for (i = 0; i < p.order; ++i)
    {
        p.x[i] = pol_find_root(q, 0, 150);
        pol_factorize(q, p.x[i], &r);
        pol_free(q);
        pol_copy(r, &q);
        pol_free(r);
    }
    pol_free(q);
}
