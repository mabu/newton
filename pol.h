
#ifndef H_20210130_POL
#define H_20210130_POL
typedef struct polynom {
    size_t order;
    double complex *a;
    double complex *x;
} pol_t;


#define CDISP(z) creal((z)), cimag((z))
#define CFMT "%.2f+%.2fi"

double randd();

void pol_init(pol_t * pol, size_t order);

void pol_free(pol_t p);

void pol_dump(pol_t pol, char *name);

double complex pol_eval(pol_t p, double complex x);

void pol_get_derivative(pol_t p, pol_t * q);

void pol_factorize(pol_t p, double complex root, pol_t *q);

double complex pol_find_root(pol_t p, double complex hint, size_t retry);

void pol_copy(pol_t p, pol_t *q);

void pol_find_all_roots(pol_t p);
#endif
