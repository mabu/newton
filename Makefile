all: test image-test newton rnewton

SRC:=$(wildcard *c)
HDR:=$(wildcard *h)
OBJ:=$(patsubst %.c,%.o,${SRC})

CFLAGS := -Wall -Wextra -O3
LDFLAGS := -lfreeimage -lm

rnewton: pol.o ray.o utils.o
	gcc ${CFLAGS}  $^ -o $@  -lm -lraylib

newton: newton.o pol.o
	gcc ${CFLAGS}  $^ -o $@  ${LDFLAGS}

image-test: image-test.o
	gcc ${CFLAGS}  $^ -o $@  ${LDFLAGS}

test: test.o pol.o
	gcc -Wall -Wextra $^ -o $@ -lm

%.o: %.c ${HDR}
	gcc -c -Wall -Wextra $< -o $@
