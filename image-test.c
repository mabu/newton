#include <FreeImage.h>
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    FreeImage_Initialise(0);
    printf("Using freeimage %s\n", FreeImage_GetVersion());
    
    FIBITMAP *im =  FreeImage_Allocate (640, 480, 24, 0, 0, 0);

    if (NULL == im) {
        perror("FreeImage_Allocate");
        exit(1);
    }

    size_t i, j;
    RGBQUAD color = { 0, 0, 255, 0 };
    for (j=100;j<300;++j)
    {
        for (i = 0; i < j;++i) 
        {
            if(!FreeImage_SetPixelColor(im, i, j, &color)) {
                perror("setpixelcolor");
            }
        }
    }
    puts("Color set");
    if (FreeImage_Save(FIF_PNG, im, "test.png", PNG_DEFAULT))
        puts("Png saved");
    if (FreeImage_Save(FIF_JPEG, im, "test.jpg", JPEG_DEFAULT))
        puts("jpg saved");
    FreeImage_Unload(im);
    FreeImage_DeInitialise();
    return 0;
}
