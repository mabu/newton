#include <math.h>
#include <stdio.h>
#include <complex.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "pol.h"
int
main(void)
{
    srand(time(NULL));

    int order = 2+rand()%4;

    pol_t pol, der;
    pol_init(&pol, order);

    pol_find_all_roots(pol);
    pol_dump(pol, "P");

//    puts("Derivative:");
//    pol_get_derivative(pol, &der);
//    pol_find_all_roots(der);
//    pol_dump(der, "Q");
/*

    complex double x = rand() % 10;
    complex double y = pol_eval(pol, x);
    printf("P("CFMT") = "CFMT"\n", CDISP(x), CDISP(y));

    puts("Find root:");
    x = pol_find_root(pol, 0);
    y = pol_eval(pol, x);
    printf("P("CFMT") = "CFMT"\n", CDISP(x), CDISP(y));

*/
    return 0;
}

