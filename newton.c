#include <FreeImage.h>
#include <stdio.h>
#include <stdlib.h>

#include <math.h>
#include <complex.h>
#include <time.h>
#include <string.h>

#include "pol.h"

#define WIDTH 1920
#define HEIGTH 975

int main(void)
{
    srand(time(NULL));

    int order = 2+rand()%4;

    RGBQUAD colors[] = 
    {
        {255,255,0, 0},
        {0,255,0 , 0},
        {255,0,0, 0},
        {0,255,255, 0},
        {255,0,255, 0}

    };
    
    pol_t pol;
    pol_init(&pol, order);

    pol_find_all_roots(pol);
    pol_dump(pol, "P");

    FreeImage_Initialise(0);
    printf("Using freeimage %s\n", FreeImage_GetVersion());
    
    FIBITMAP *im =  FreeImage_Allocate (WIDTH, HEIGTH, 24, 0, 0, 0);

    if (NULL == im) {
        perror("FreeImage_Allocate");
        exit(1);
    }

    size_t i, j, k;
    RGBQUAD color = { 0, 0, 255, 0 };
    double hw = WIDTH/2.;
    double hh = HEIGTH/2.;

    size_t progress_old = 0, progress;

    printf("[...................]\r[");
    fflush(stdout);


    for (j=0;j<HEIGTH;++j)
    {
        for (i = 0; i < WIDTH;++i) 
        {
            complex double z;
            z = (i-hw)/10. + I*(j-hh)/10.;
            z = pol_find_root(pol, z, 0);
            if (isnan(creal(z)+cimag(z))) {
                color.rgbRed = 255;
                color.rgbBlue = 0;
                color.rgbGreen = 0;
            } 
            else 
            {
                for (k = 0; k < 5;++k){
                    if(cabs(pol.x[k]-z)<.1)
                    {
                        memcpy(&color, colors+k, sizeof color);
                    }      
                }  
            }
            FreeImage_SetPixelColor(im, i, j, &color);
        }
        
        progress = j*20/HEIGTH;
        if (progress_old != progress)
        {
            progress_old = progress;
            printf("*");
            fflush(stdout);
        }
    }
    if (FreeImage_Save(FIF_PNG, im, "newton.png", PNG_DEFAULT))
        puts("]\nPng saved");
    FreeImage_Unload(im);
    FreeImage_DeInitialise();
    return 0;
}
