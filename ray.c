/*******************************************************************************************
*
*   raylib [physac] example - physics demo
*
*   This example has been created using raylib 1.5 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   This example uses physac 1.1 (https://github.com/raysan5/raylib/blob/master/src/physac.h)
*
*   Copyright (c) 2016-2021 Victor Fisac (@victorfisac) and Ramon Santamaria (@raysan5)
*
********************************************************************************************/



#include <stdio.h>
#include <stdlib.h>

#include <math.h>
#include <complex.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

#include <raylib.h>

#include "pol.h"
#include "utils.h"

#define WIDTH 1800
#define HEIGTH 900


int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = WIDTH;
    const int screenHeight = HEIGTH;

    srand(time(NULL));

    int order = 2 + rand() % 4;

    Color colors[] = {
        ORANGE,
        PINK,
        RED,
        SKYBLUE,
        PURPLE
    };

    pol_t pol;
    pol_init(&pol, order);

    pol_find_all_roots(pol);

    SetConfigFlags(FLAG_MSAA_4X_HINT);
    InitWindow(screenWidth, screenHeight, "Newton");

    SetTargetFPS(60);           // Set our game to run at 60 frames-per-second
    {


        size_t i, j, k;
        size_t seed = 1;
        size_t cnt = 0;
        Color color = { 0, 0, 255, 0 };
        double hw = WIDTH / 2.;
        double hh = HEIGTH / 2.;


        BeginDrawing();

        do {
            fizzle_next(&seed, &i, &j);
            if (i < WIDTH && j < HEIGTH) {

                complex double z;
                z = (i - hw) / 10. + I * (j - hh) / 10.;
                z = pol_find_root(pol, z, 0);
                if (isnan(creal(z) + cimag(z))) {
                    color = WHITE;
                } else {
                    for (k = 0; k < 5; ++k) {
                        if (cabs(pol.x[k] - z) < .1) {
                            color = colors[k];
                        }
                    }
                }
                DrawPixel(i, j, color);
                ++cnt;
            }
            if (!(cnt % 20000)) {
                EndDrawing();
                if (WindowShouldClose())
                    break;
                BeginDrawing();

            }
        } while (seed != 1);
        EndDrawing();
    }

    TakeScreenshot("rnewton.png");
    sleep(10);
    CloseWindow();              // Close window and OpenGL context

    return 0;
}
